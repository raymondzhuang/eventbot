import * as builder from 'botbuilder';
import * as _ from 'underscore';


function processSubmitAction(session: builder.Session, value) {
    //get values from the JSON result
    console.log(value);
    var eventName = value[INPUT_EVENT];
    var date = value[INPUT_DATE];
    var performers = value[INPUT_PERFORMERS];

    // client.execute is asynchronous (at least I think so) so each statement is executed in nested callback functions
    client.execute(`g.addV('${EVENT_LABEL}').property('id', '${eventName}').property('eventName', '${eventName}')`, function (err, results) {
        if (!err) {
            console.log(results) // Handle an array of results 
        } else {
            session.send("failed to add event");
        }

        client.execute(`g.addV('${DATE_LABEL}').property('id', '${date}').property('date', '${date}')`, function (err, results) {
            if (!err) {
                console.log(results) // Handle an array of results 
            } else {
                console.error(err);
                session.send("failed to add event date");
            }

            client.execute(`g.addV('${PERFORMERS_LABEL}').property('id', '${performers}').property('name', '${performers}')`, function (err, results) {
                if (!err) {
                    console.log(results) // Handle an array of results 
                } else {
                    console.error(err);
                    session.send("failed to add event performers");
                }

                client.execute(`g.V('${eventName}').addE('on').to(g.V('${date}'))`, function (err, results) {
                    if (!err) {
                        console.log(results) // Handle an array of results 
                    } else {
                        console.error(err);
                        session.send("failed to add edge");
                    }
                    client.execute(`g.V('${performers}').addE('performing').to(g.V('${eventName}'))`, function (err, results) {
                        if (!err) {
                            console.log(results) // Handle an array of results 
                        } else {
                            console.error(err);
                            session.send("failed to add edge");
                        }
                    });
                });
            });
        });
    });

    session.send("Event name: %s \n\n Date: %s \n\n Performers: %s", eventName, date, performers);
    session.endDialog("Done!");
}


export class sign_up {
    dialog_name = 'signUp';

    search =
    (session: builder.Session) => {
        session.send("searching...");
        var events;
        client.execute(`g.V().hasLabel('${EVENT_LABEL}').values('eventName')`, function (err, results) {
            if (!err) {
                events = results;
                session.send(results.toString());
                console.log(results);
                builder.Prompts.choice(session, "Which event would you like to sign up for", results, { listStyle: builder.ListStyle.button });
            } else {
                session.endDialog("No events found");
                console.error(err);
            }
        })
        // session.replaceDialog('prompt');
    };

    update =
    (session: builder.Session) => {
        client.execute(`g.V('${session.message.text}').values('numAttending')`, function (err, results) {
            if (!err) {
                console.log(results[0]);
                client.execute(`g.V('${session.message.text}').property('numAttending', ${++results[0]})`, function (err, results) {
                    if (!err) {
                        console.log(results[0].properties.numAttending);
                        session.endDialog("Great! You have signed up for " + session.message.text);
                    } else {
                        session.send("Couldn't sign up");
                    }
                });
            } else {
                console.log(err);
                session.endDialog("could not find event");
            }
        });
    };

    dialog_stack = [this.search, this.update];
}

export class sign_up_from_LUIS {
    dialog_name = 'signUpFromLUIS';

    search =
    (session: builder.Session) => {
        builder.Prompts.choice(session, "Looks like you've already told me about that. Are you going?", "Yes|No", { listStyle: builder.ListStyle.button });
        // session.replaceDialog('prompt');
    };

    update =
    (session: builder.Session, results) => {
        client.execute(`g.V('${session.conversationData.eventName}').values('numAttending')`, function (err, results) {
            if (!err) {
                console.log(results[0]);
                client.execute(`g.V('${session.conversationData.eventName}').property('numAttending', ${++results[0]})`, function (err, results) {
                    if (!err) {
                        // console.log(results[0].properties.numAttending);
                        session.endDialog("Great! You have signed up for " + session.conversationData.eventName);
                    } else {
                        session.send("Couldn't sign up");
                    }
                });
            } else {
                console.log(err);
                session.endDialog("could not find event");
            }
        });
    };

    dialog_stack = [this.search, this.update];
}

export class learn_event {
    dialog_name = 'learnEvent';

    learn =
    (session: builder.Session) => {
        if (session.message && session.message.value) {
            processSubmitAction(session, session.message.value);
            session.endDialog("all done");
            return;
        }

        var card = {
            'contentType': 'application/vnd.microsoft.card.adaptive',
            'content': {
                'type': 'AdaptiveCard',
                "body": [
                    {
                        "type": "ColumnSet",
                        "columns": [
                            {
                                "type": "Column",
                                "width": 1,
                                "items": [
                                    {
                                        "type": "TextBlock",
                                        "text": "Tell us about the event",
                                        "weight": "bolder",
                                        "size": "medium"
                                    },
                                    {
                                        "type": "TextBlock",
                                        "text": "Event name",
                                        "wrap": true
                                    },
                                    {
                                        "type": "Input.Text",
                                        "id": INPUT_EVENT,
                                        "placeholder": "Event name"
                                    },
                                    {
                                        "type": "TextBlock",
                                        "text": "Date of Event",
                                        "wrap": true
                                    },
                                    {
                                        "type": "Input.Date",
                                        "id": INPUT_DATE,
                                        "placeholder": "dd/MM/yyyy"
                                    },
                                    {
                                        "type": "TextBlock",
                                        "text": "Performer"
                                    },
                                    {
                                        "type": "Input.Text",
                                        "id": INPUT_PERFORMERS
                                    }
                                ]
                            }
                        ]
                    }
                ],
                "actions": [
                    {
                        "type": "Action.Submit",
                        "title": "Submit"
                    }
                ]
            }
        };

        var msg = new builder.Message(session)
            .addAttachment(card);

        session.send(msg);
    }
    dialog_stack = [this.learn];
}

