// disclaimer: not actually type script...
var restify = require('restify');
var builder = require('botbuilder');
var config = require('./config');
var azure = require('botbuilder-azure');
var Gremlin = require('gremlin');
var chrono = require('chrono-node')
// import * as builder from 'botbuilder';



const client = Gremlin.createClient(
    443,
    config.endpoint,
    {
        "session": false,
        "ssl": true,
        "user": `/dbs/${config.database}/colls/${config.collection}`,
        "password": config.primaryKey
    });

// put in .env later
const EVENT_LABEL = "event";
const DATE_LABEL = "date";
const PERFORMERS_LABEL = "performers";

const INPUT_EVENT = "eventName";
const INPUT_DATE = "date";
const INPUT_PERFORMERS = "performers";

const LUIS_ENDPOINT = "https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/dda5f499-c295-465c-9744-c5cd9ed9e522?subscription-key=d71ce16359bb4b289c311d2cbd469791&timezoneOffset=0&verbose=true&q=";

// Setup Restify Server
const server = restify.createServer();
server.listen(process.env.port || process.env.PORT || 3978, function () {
    console.log('%s listening to %s', server.name, server.url);
});

// Create chat connector for communicating with the Bot Framework Service
var connector = new builder.ChatConnector({
    appId: process.env.MICROSOFT_APP_ID,
    appPassword: process.env.MICROSOFT_APP_PASSWORD
});

// Listen for messages from users 
server.post('/api/messages', connector.listen());

var bot = new builder.UniversalBot(connector, function (session) {
    session.beginDialog('prompt');
});


// Add global LUIS recognizer to bot
var luisAppUrl = process.env.LUIS_APP_URL || LUIS_ENDPOINT;
bot.recognizer(new builder.LuisRecognizer(luisAppUrl));

bot.dialog('prompt', [
    function (session) {
        builder.Prompts.choice(session, "What would you like to do?", "Sign up for event|Tell you about an event", { listStyle: builder.ListStyle.button });
    },
    function (session) {
        console.log(session.message);
        switch (session.message.text) {
            case "Sign up for event":
                session.beginDialog('signUp');
                break;
            case "Tell you about an event":
                session.beginDialog('learnEvent');
                break;
        }
        // session.replaceDialog('prompt');
    }
]);

bot.dialog('resetGraph', function (session) {
    client.execute('g.V().drop()', function (err, results) {
        if (!err) {
            console.log(results) // Handle an array of results 
            session.send("reset");
        } else {
            session.send("failed to drop");
        }
    })
}).triggerAction({ matches: /^reset/i });

bot.dialog('signUp', [
    function (session) {
        session.send("searching...");
        var events;
        client.execute(`g.V().hasLabel('${EVENT_LABEL}').values('eventName')`, function (err, results) {
            if (!err) {
                events = results;
                session.send(results.toString());
                console.log(results);
                builder.Prompts.choice(session, "Which event would you like to sign up for", results, { listStyle: builder.ListStyle.button });
            } else {
                session.endDialog("No events found");
                console.error(err);
            }
        })
        // session.replaceDialog('prompt');
    }, function (session) {
        client.execute(`g.V('${session.message.text}').values('numAttending')`, function (err, results) {
            if (!err) {
                console.log(results[0]);
                client.execute(`g.V('${session.message.text}').property('numAttending', ${++results[0]})`, function (err, results) {
                    if (!err) {
                        console.log(results[0].properties.numAttending);
                        session.endDialog("Great! You have signed up for " + session.message.text);
                    } else {
                        session.send("Couldn't sign up");
                    }
                });
            } else {
                console.log(err);
                session.endDialog("could not find event");
            }
        });
    }
]);

bot.dialog('signUpFromLUIS', [
    function (session) {
        builder.Prompts.choice(session, "Looks like you've already told me about that. Are you going?", "Yes|No", { listStyle: builder.ListStyle.button });
        // session.replaceDialog('prompt');
    }, function (session, results) {
        client.execute(`g.V('${session.conversationData.eventName}').values('numAttending')`, function (err, results) {
            if (!err) {
                console.log(results[0]);
                client.execute(`g.V('${session.conversationData.eventName}').property('numAttending', ${++results[0]})`, function (err, results) {
                    if (!err) {
                        // console.log(results[0].properties.numAttending);
                        session.endDialog("Great! You have signed up for " + session.conversationData.eventName);
                    } else {
                        session.send("Couldn't sign up");
                    }
                });
            } else {
                console.log(err);
                session.endDialog("could not find event");
            }
        });
    }
]);

bot.dialog('learnEvent', [
    function (session) {
        if (session.message && session.message.value) {
            processSubmitAction(session, session.message.value);
            session.endDialog("all done");
            return;
        }

        var card = {
            'contentType': 'application/vnd.microsoft.card.adaptive',
            'content': {
                'type': 'AdaptiveCard',
                "body": [
                    {
                        "type": "ColumnSet",
                        "columns": [
                            {
                                "type": "Column",
                                "width": 1,
                                "items": [
                                    {
                                        "type": "TextBlock",
                                        "text": "Tell us about the event",
                                        "weight": "bolder",
                                        "size": "medium"
                                    },
                                    {
                                        "type": "TextBlock",
                                        "text": "Event name",
                                        "wrap": true
                                    },
                                    {
                                        "type": "Input.Text",
                                        "id": INPUT_EVENT,
                                        "placeholder": "Event name"
                                    },
                                    {
                                        "type": "TextBlock",
                                        "text": "Date of Event",
                                        "wrap": true
                                    },
                                    {
                                        "type": "Input.Date",
                                        "id": INPUT_DATE,
                                        "placeholder": "dd/MM/yyyy"
                                    },
                                    {
                                        "type": "TextBlock",
                                        "text": "Performer"
                                    },
                                    {
                                        "type": "Input.Text",
                                        "id": INPUT_PERFORMERS
                                    }
                                ]
                            }
                        ]
                    }
                ],
                "actions": [
                    {
                        "type": "Action.Submit",
                        "title": "Submit"
                    }
                ]
            }
        };

        var msg = new builder.Message(session)
            .addAttachment(card);

        session.send(msg);
    }
]);

function determineDateType(entities) {
    var dateArray = (entities.filter(function (x) { return x.type == "builtin.datetimeV2.date" }));
    var dateTimeArray = (entities.filter(function (x) { return x.type == "builtin.datetimeV2.datetime" }));
}

// handle whether or not to signup or register an event in the graph
// bot.dialog('handleInput',
//     function (session, args, next) {
//         var intent = args.intent;
//         var eventNameObject = builder.EntityRecognizer.findEntity(intent.entities, 'Event.Name');
//         //need to handle this date time to produce an actual date
//         var dateObject = builder.EntityRecognizer.findEntity(intent.entities, 'builtin.datetimeV2.date');
//         var performersObject = builder.EntityRecognizer.findEntity(intent.entities, 'Event.Act');
//     }
// ).triggerAction({
//     matches: 'Events.Register'
// });

// LUIS dialog
bot.dialog('learnEventOther', [
    function (session, args, next) {

        if (session.message && session.message.value) {
            processSubmitAction(session, session.message.value);
            session.endDialog("all done");
            return;
        }

        var intent = args.intent;
        determineDateType(intent.entities);
        var eventNameObject = builder.EntityRecognizer.findEntity(intent.entities, 'Event.Name');
        //need to handle this date time to produce an actual date
        var dateObject = builder.EntityRecognizer.findEntity(intent.entities, 'builtin.datetimeV2.date');
        var performersObject = builder.EntityRecognizer.findEntity(intent.entities, 'Event.Act');

        if (eventNameObject != null) {
            client.execute(`g.V('${eventNameObject.entity}')`, function (err, results) {
                if (!err) {
                    console.log("Results are" + results);
                    if (results.length > 0) {
                        session.conversationData.eventName = eventNameObject.entity;
                        session.replaceDialog('signUpFromLUIS');
                    } else {
                        console.log("errorrrrrrrrrrrrrrrr");

                        var eventName = eventNameObject ? eventNameObject.entity : null;
                        var date = dateObject ? dateObject.resolution.values[0].timex : null;
                        var performers = performersObject ? performersObject.entity : null;


                        // console.log(intent);
                        // console.log(eventNameObject);
                        // console.log(chrono.parseDate("next tuesday"));
                        // console.log(date);
                        // console.log(performersObject);

                        var card = {
                            'contentType': 'application/vnd.microsoft.card.adaptive',
                            'content': {
                                'type': 'AdaptiveCard',
                                "body": [
                                    {
                                        "type": "ColumnSet",
                                        "columns": [
                                            {
                                                "type": "Column",
                                                "width": 1,
                                                "items": [
                                                    {
                                                        "type": "TextBlock",
                                                        "text": "Does this look right?",
                                                        "weight": "bolder",
                                                        "size": "medium"
                                                    },
                                                    {
                                                        "type": "TextBlock",
                                                        "text": "Event name",
                                                        "wrap": true
                                                    },
                                                    {
                                                        "type": "Input.Text",
                                                        "id": INPUT_EVENT,
                                                        "placeholder": "Event Name",
                                                        "value": eventName
                                                    },
                                                    {
                                                        "type": "TextBlock",
                                                        "text": "Date of Event",
                                                        "wrap": true
                                                    },
                                                    {
                                                        "type": "Input.Date",
                                                        "id": INPUT_DATE,
                                                        "placeholder": "dd/MM/yyyy",
                                                        "value": `${chrono.parseDate("next tuesday")}`
                                                    },
                                                    {
                                                        "type": "TextBlock",
                                                        "text": "Performer"
                                                    },
                                                    {
                                                        "type": "Input.Text",
                                                        "id": INPUT_PERFORMERS,
                                                        "placeholder": "Performers",
                                                        "value": performers
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ],
                                "actions": [
                                    {
                                        "type": "Action.Submit",
                                        "title": "Submit"
                                    }
                                ]
                            }
                        };

                        console.log(card.content.body[0].columns[0].items);
                        var msg = new builder.Message(session)
                            .addAttachment(card);

                        session.send(msg);

                    }
                } else {
                    var eventName = eventNameObject ? eventNameObject.entity : null;
                    var date = dateObject ? dateObject.resolution.values[0].timex : null;
                    var performers = performersObject ? performersObject.entity : null;


                    // console.log(intent);
                    // console.log(eventNameObject);
                    // console.log(chrono.parseDate("next tuesday"));
                    // console.log(date);
                    // console.log(performersObject);

                    var card = {
                        'contentType': 'application/vnd.microsoft.card.adaptive',
                        'content': {
                            'type': 'AdaptiveCard',
                            "body": [
                                {
                                    "type": "ColumnSet",
                                    "columns": [
                                        {
                                            "type": "Column",
                                            "width": 1,
                                            "items": [
                                                {
                                                    "type": "TextBlock",
                                                    "text": "Does this look right?",
                                                    "weight": "bolder",
                                                    "size": "medium"
                                                },
                                                {
                                                    "type": "TextBlock",
                                                    "text": "Event name",
                                                    "wrap": true
                                                },
                                                {
                                                    "type": "Input.Text",
                                                    "id": INPUT_EVENT,
                                                    "placeholder": "Event Name",
                                                    "value": eventName
                                                },
                                                {
                                                    "type": "TextBlock",
                                                    "text": "Date of Event",
                                                    "wrap": true
                                                },
                                                {
                                                    "type": "Input.Date",
                                                    "id": INPUT_DATE,
                                                    "placeholder": "dd/MM/yyyy",
                                                    "value": `${chrono.parseDate("next tuesday")}`
                                                },
                                                {
                                                    "type": "TextBlock",
                                                    "text": "Performer"
                                                },
                                                {
                                                    "type": "Input.Text",
                                                    "id": INPUT_PERFORMERS,
                                                    "placeholder": "Performers",
                                                    "value": performers
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ],
                            "actions": [
                                {
                                    "type": "Action.Submit",
                                    "title": "Submit"
                                }
                            ]
                        }
                    };

                    console.log(card.content.body[0].columns[0].items);
                    var msg = new builder.Message(session)
                        .addAttachment(card);

                    session.send(msg);
                }
            })
        } else {
            var eventName = eventNameObject ? eventNameObject.entity : null;
            var date = dateObject ? dateObject.resolution.values[0].timex : null;
            var performers = performersObject ? performersObject.entity : null;


            // console.log(intent);
            // console.log(eventNameObject);
            // console.log(chrono.parseDate("next tuesday"));
            // console.log(date);
            // console.log(performersObject);

            var card = {
                'contentType': 'application/vnd.microsoft.card.adaptive',
                'content': {
                    'type': 'AdaptiveCard',
                    "body": [
                        {
                            "type": "ColumnSet",
                            "columns": [
                                {
                                    "type": "Column",
                                    "width": 1,
                                    "items": [
                                        {
                                            "type": "TextBlock",
                                            "text": "Does this look right?",
                                            "weight": "bolder",
                                            "size": "medium"
                                        },
                                        {
                                            "type": "TextBlock",
                                            "text": "Event name",
                                            "wrap": true
                                        },
                                        {
                                            "type": "Input.Text",
                                            "id": INPUT_EVENT,
                                            "placeholder": "Event Name",
                                            "value": eventName
                                        },
                                        {
                                            "type": "TextBlock",
                                            "text": "Date of Event",
                                            "wrap": true
                                        },
                                        {
                                            "type": "Input.Date",
                                            "id": INPUT_DATE,
                                            "placeholder": "dd/MM/yyyy",
                                            "value": `${chrono.parseDate("next tuesday")}`
                                        },
                                        {
                                            "type": "TextBlock",
                                            "text": "Performer"
                                        },
                                        {
                                            "type": "Input.Text",
                                            "id": INPUT_PERFORMERS,
                                            "placeholder": "Performers",
                                            "value": performers
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    "actions": [
                        {
                            "type": "Action.Submit",
                            "title": "Submit"
                        }
                    ]
                }
            };

            console.log(card.content.body[0].columns[0].items);
            var msg = new builder.Message(session)
                .addAttachment(card);

            session.send(msg);
        }
    }
]).triggerAction({
    matches: 'Events.Register'
});

// used to get the values from the adaptive card
function processSubmitAction(session, value) {

    //get values from the JSON result
    console.log(value);
    var eventName = value[INPUT_EVENT];
    var date = value[INPUT_DATE];
    var performers = value[INPUT_PERFORMERS];

    // client.execute is asynchronous (at least I think so) so each statement is executed in nested callback functions
    client.execute(`g.addV('${EVENT_LABEL}').property('id', '${eventName}').property('eventName', '${eventName}')`, function (err, results) {
        if (!err) {
            console.log(results) // Handle an array of results 
        } else {
            session.send("failed to add event");
        }

        client.execute(`g.addV('${DATE_LABEL}').property('id', '${date}').property('date', '${date}')`, function (err, results) {
            if (!err) {
                console.log(results) // Handle an array of results 
            } else {
                console.error(err);
                session.send("failed to add event date");
            }

            client.execute(`g.addV('${PERFORMERS_LABEL}').property('id', '${performers}').property('name', '${performers}')`, function (err, results) {
                if (!err) {
                    console.log(results) // Handle an array of results 
                } else {
                    console.error(err);
                    session.send("failed to add event performers");
                }

                client.execute(`g.V('${eventName}').addE('on').to(g.V('${date}'))`, function (err, results) {
                    if (!err) {
                        console.log(results) // Handle an array of results 
                    } else {
                        console.error(err);
                        session.send("failed to add edge");
                    }
                    client.execute(`g.V('${performers}').addE('performing').to(g.V('${eventName}'))`, function (err, results) {
                        if (!err) {
                            console.log(results) // Handle an array of results 
                        } else {
                            console.error(err);
                            session.send("failed to add edge");
                        }
                    });
                });
            });
        });
    });

    session.send("Event name: %s \n\n Date: %s \n\n Performers: %s", eventName, date, performers);
    session.endDialog("Done!");
}